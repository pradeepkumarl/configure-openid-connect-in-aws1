aws_region      = "ap-south-1"
gitlab_url      = "https://gitlab.example.com"
aud_value       = "https://gitlab.example.com"
match_field     = "sub"
match_value     = ["https://gitlab.com/pradeepkumarl/*:ref_type:branch:ref:main"]
assume_role_arn = ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]
